

$(document).ready(function () {
    $('#abrir').click(function () {
        $('#menutopo').addClass("ativo");
    });

    $('#fecha').click(function () {
        $('#menutopo').removeClass("ativo");
    });
});




// hover home
$( document ).ready(function() {  
    $('.fotoLoop').hover(
        function(){
            $(this).find('.hovergal').fadeIn(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.hovergal').fadeOut(250); //.fadeOut(205)
        }
    ); 
});


// hover galeria
$( document ).ready(function() {  
    $('.fotosGaleria').hover(
        function(){
            $(this).find('.hovergal').fadeIn(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.hovergal').fadeOut(250); //.fadeOut(205)
        }
    ); 
});


/*OWL*/

$(document).ready(function() {
 
  $("#owl-example").owlCarousel({

    // Most important owl features
    items : 3,
    itemsCustom : false,
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    singleItem : false,
    itemsScaleUp : false,

    // Navigation
    navigation : true,
    navigationText : ["<",">"],
    rewindNav : true,
    scrollPerPage : false,

    //Pagination
    pagination : true,
    paginationNumbers: true,

    // CSS Styles
    baseClass : "owl-carousel",
    theme : "owl-galeria",

  });
 
});